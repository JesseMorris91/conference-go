from django.views.decorators.http import require_http_methods

from .acls import get_photo, get_weather_data

from common.json import ModelEncoder

from .models import Conference, Location, State

from django.http import JsonResponse

import json


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = ["name", "city", "room_count", "created", "updated", "state"]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    # GET a locations details
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            safe=False,
        )
    else:
        # CREATE a location
        content = json.loads(request.body)
        # make a request to pexels passing in the city and state
        # clean that data for the image
        # add that image to my lcoation model
        # create the location

        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):

    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        weather = get_weather_data(
            conference.location.city,
            conference.location.state.abbreviation,
        )

        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        # gives queryset containing object that matches lookup parameter
        # and deletes
        return JsonResponse({"deleted": count > 0})

    content = json.loads(request.body)

    Conference.objects.filter(id=id).update(**content)

    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )

    # content = json.loads(request.body)

    # Conference.objects.filter(id=id).update(**content)

    # conference = Conference.objects.get(id=id)
    # return JsonResponse(
    #     conference,
    #     encoder=ConferenceDetailEncoder,
    #     safe=False,
    # )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()

        return JsonResponse(
            {"locations": locations}, encoder=LocationListEncoder, safe=False
        )

        # loading content of POST request
        # POST is a json formatted string stored in request.body
    else:
        content = json.loads(request.body)

        # look up state based on value in content dict in "state" key
        # assign it back to the dictionary
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

    # using get_photo function to get url of a pic for
    # the city when using POST
    photo = get_photo(content["city"], content["state"].abbreviation)
    content.update(photo)
    # takes everything in dictionary and creates keyword arguements
    location = Location.objects.create(**content)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )

    # response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append(
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #             "state": {
    #                 "name": location.state.name,
    #                 "abbreviation": location.state.abbreviation,
    #             },
    #         }
    #     )

    # return JsonResponse({"locations": response})


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:  # makes into dictionary for "PUT" update
        content = json.loads(request.body)

        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    # location = Location.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "name": location.name,
    #         "city": location.city,
    #         "room_count": location.room_count,
    #         "created": location.created,
    #         "updated": location.updated,
    #         "state": location.state.abbreviation,
    #     }
    # )
