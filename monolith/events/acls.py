from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # API required headers / Auth & api key
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"downtown {city} {state}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content[0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # all objects we want to request from API
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    # API url for geo coordinates
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # requesting geo-coding from API
    response = requests.get(url, params=params)
    # Parsing JSON response / receiving data back from API, takes up
    # a file object and returns JSON data converted into a python object
    content = json.loads(response.content)

    # temporarily storing the data in lat/lon from the response
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # using our new lat/lon variables that now have data we received from API
    # so we can send back coordinates to API to get correct
    # weather in that area
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    # API url to get current weather data
    url = "https://api.openweathermap.org/data/2.5/weather"
    # requesting the current weather data with our lat/lon coordinates
    response = requests.get(url, params=params)
    # Parsing JSON response / receiving current weather data back from API
    content = json.loads(response.content)
    # getting the main temp and weathers description and
    # storing in a dictionary
    try:
        return {
            "Temperature": content["main"]["temp"],
            "Weather": content["weather"][0]["description"],
        }

    except (KeyError, IndexError):
        return None
